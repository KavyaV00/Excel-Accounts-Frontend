import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Sidebar from '../components/common/Sidebar/Sidebar';
import Navbar from '../components/common/Navbar/Navbar';

import Home from './Home/Home';
import PrivateRoute from '../components/common/PrivateRoute/PrivateRoute';

const Base = () => (
  <PrivateRoute>
    <div className="App">
      <div className="wrapper ">
        <Sidebar />
        <div className="main-panel">
          <Navbar />
          <div className="content">
            <div className="container-fluid">
              <Switch>
                <Route exact path="/" component={Home} />
              </Switch>
            </div>
          </div>
        </div>
      </div>
    </div>
  </PrivateRoute>
);

export default Base;