// import React from 'react';

const isLoggedIn = () => localStorage.getItem('jwt_token') ? true : false;

interface IProps {
  children: any
}

const PrivateRoute = ({children}:IProps) => {
  if (isLoggedIn()) {
    return children;
  }
  window.location.href=`${window.location.origin}/auth/login?redirect_to=${window.location.href}`;
  return null;
}

export default PrivateRoute;